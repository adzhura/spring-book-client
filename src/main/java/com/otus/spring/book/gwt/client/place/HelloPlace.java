package com.otus.spring.book.gwt.client.place;


import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;


public class HelloPlace extends Place
{
    public HelloPlace()
    {
    }

    public static class Tokenizer implements PlaceTokenizer<HelloPlace>
    {
        @Override
        public String getToken(HelloPlace place)
        {
            return "";
        }

        @Override
        public HelloPlace getPlace(String token)
        {
            return new HelloPlace();
        }
    }
}