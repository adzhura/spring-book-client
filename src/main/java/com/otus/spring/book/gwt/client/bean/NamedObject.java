package com.otus.spring.book.gwt.client.bean;


public interface NamedObject<T> extends HasId<T>
{
    String getName();
}
