package com.otus.spring.book.gwt.client.hello;


import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.IsWidget;


public interface HelloView extends IsWidget
{
    void setPresenter(Presenter presenter);

    public interface Presenter
    {
        void goTo(Place place);
    }
}
