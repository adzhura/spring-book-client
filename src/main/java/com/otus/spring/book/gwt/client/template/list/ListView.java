package com.otus.spring.book.gwt.client.template.list;


import java.util.function.Consumer;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.view.client.HasData;


public interface ListView<T> extends IsWidget
{
    HasData<T> dataDisplay();
    
    HasValue<String> findText();
    
    HasClickHandlers findAction();
    
    HasClickHandlers resetAction();
    
    HasClickHandlers createAction();
    
    void setEditHandler(Consumer<T> consumer);

    void setDeleteHandler(Consumer<T> consumer);
}