package com.otus.spring.book.gwt.client.form.genre.edit;


import com.google.gwt.user.client.ui.HasValue;
import com.otus.spring.book.gwt.client.template.edit.EditView;


public interface GenreEditView extends EditView
{
    HasValue<String> idDisplay();

    HasValue<String> nameDisplay();
}