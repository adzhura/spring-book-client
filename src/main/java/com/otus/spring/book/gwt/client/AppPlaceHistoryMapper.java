package com.otus.spring.book.gwt.client;


import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;
import com.otus.spring.book.gwt.client.place.AuthorEditPlace;
import com.otus.spring.book.gwt.client.place.AuthorListPlace;
import com.otus.spring.book.gwt.client.place.BookCommentsPlace;
import com.otus.spring.book.gwt.client.place.BookEditPlace;
import com.otus.spring.book.gwt.client.place.BookListPlace;
import com.otus.spring.book.gwt.client.place.GenreEditPlace;
import com.otus.spring.book.gwt.client.place.GenreListPlace;
import com.otus.spring.book.gwt.client.place.HelloPlace;


@WithTokenizers({HelloPlace.Tokenizer.class, GenreListPlace.Tokenizer.class,
    GenreEditPlace.Tokenizer.class, AuthorListPlace.Tokenizer.class,
    AuthorEditPlace.Tokenizer.class, BookListPlace.Tokenizer.class,
    BookEditPlace.Tokenizer.class, BookCommentsPlace.Tokenizer.class})
public interface AppPlaceHistoryMapper extends PlaceHistoryMapper
{
}