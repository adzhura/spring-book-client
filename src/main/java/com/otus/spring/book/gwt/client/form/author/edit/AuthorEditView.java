package com.otus.spring.book.gwt.client.form.author.edit;


import com.google.gwt.user.client.ui.HasValue;
import com.otus.spring.book.gwt.client.template.edit.EditView;


public interface AuthorEditView extends EditView
{
    HasValue<String> idDisplay();

    HasValue<String> firstNameDisplay();

    HasValue<String> lastNameDisplay();
}