package com.otus.spring.book.gwt.client.bean;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id", "firstName", "lastName"})
@ToString(of = {"id", "firstName", "lastName"})
public class Author implements HasId<String>
{
    private String id;

    private String firstName;

    private String lastName;
}
