package com.otus.spring.book.gwt.client.form.book.comments;


import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.view.client.HasData;
import com.otus.spring.book.gwt.client.bean.Comment;


public interface BookCommentsView extends IsWidget
{
    HasText nameDisplay();
    
    HasText authorDisplay();
    
    HasText genreDisplay();

    HasValue<String> userNameDisplay();
    
    HasValue<String> textDisplay();
    
    HasClickHandlers addCommentAction();
    
    HasValue<String> findText();
    
    HasClickHandlers findAction();
    
    HasClickHandlers resetAction();

    HasData<Comment> commentDisplay();
}