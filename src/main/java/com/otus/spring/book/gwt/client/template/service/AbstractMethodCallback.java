package com.otus.spring.book.gwt.client.template.service;


import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;


public abstract class AbstractMethodCallback<T> implements MethodCallback<T>
{
    @Override
    public void onFailure(Method method, Throwable exception)
    {
        Response response = method.getResponse();
        if (response.getStatusCode() == 0)
        {
            Window
                .alert("Service unavailable");
        }
        else
        {
            Window.alert(exception.getMessage());
        }
    }
}
