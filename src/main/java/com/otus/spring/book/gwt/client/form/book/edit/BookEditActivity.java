package com.otus.spring.book.gwt.client.form.book.edit;


import java.util.List;
import java.util.function.Consumer;

import org.fusesource.restygwt.client.Method;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.otus.spring.book.gwt.client.ClientFactory;
import com.otus.spring.book.gwt.client.bean.Author;
import com.otus.spring.book.gwt.client.bean.Book;
import com.otus.spring.book.gwt.client.bean.Genre;
import com.otus.spring.book.gwt.client.place.BookEditPlace;
import com.otus.spring.book.gwt.client.place.BookListPlace;
import com.otus.spring.book.gwt.client.service.AuthorService;
import com.otus.spring.book.gwt.client.service.BookService;
import com.otus.spring.book.gwt.client.service.GenreService;
import com.otus.spring.book.gwt.client.template.service.AbstractMethodCallback;


public class BookEditActivity extends AbstractActivity
{
    private ClientFactory clientFactory;
    private String id;

    public BookEditActivity(BookEditPlace place, ClientFactory clientFactory)
    {
        this.clientFactory = clientFactory;
        id = place.getId();
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus)
    {
        BookEditView view = clientFactory.getBookEditView();
        containerWidget.setWidget(view.asWidget());

        view.idDisplay().setValue(null);
        view.nameDisplay().setValue(null);
        view.authorDisplay().setValue(null);
        view.genreDisplay().setValue(null);

        if (id != null && !id.trim().isEmpty())
        {
            loadData(id, book -> {
                view.saveEnabled().setEnabled(book != null);

                if (book != null)
                {
                    view.idDisplay().setValue(book.getId());
                    view.nameDisplay().setValue(book.getName());
                    view.authorDisplay().setValue(book.getAuthor(), true);
                    view.genreDisplay().setValue(book.getGenre(), true);
                }

                updateLookupAuthor(view, r -> {
                });
                updateLookupGenre(view, r -> {
                });
            });
        }
        else
        {
            updateLookupAuthor(view, r -> {
                if (r != null && r.size() > 0)
                {
                    view.authorDisplay().setValue(r.get(0));
                }
            });
            updateLookupGenre(view, r -> {
                if (r != null && r.size() > 0)
                {
                    view.genreDisplay().setValue(r.get(0));
                }
            });
        }

        view.saveAction().addClickHandler(new ClickHandler()
        {
            @Override
            public void onClick(ClickEvent event)
            {
                String id = view.idDisplay().getValue();
                id = "".equals(id) ? null : id;

                Book book = new Book(id, view.nameDisplay().getValue(),
                    view.authorDisplay().getValue(),
                    view.genreDisplay().getValue(), null);

                saveData(book, r -> clientFactory.getPlaceController()
                    .goTo(new BookListPlace()));
            }
        });
    }

    private void updateLookupAuthor(BookEditView view,
        Consumer<List<Author>> consumer)
    {
        AuthorService.getInstance()
            .findAll(new AbstractMethodCallback<List<Author>>()
            {
                @Override
                public void onSuccess(Method method, List<Author> response)
                {
                    consumer.accept(response);
                    view.authorDisplay().setAcceptableValues(response);
                }
            });
    }

    private void updateLookupGenre(BookEditView view,
        Consumer<List<Genre>> consumer)
    {
        GenreService.getInstance()
            .findAll(new AbstractMethodCallback<List<Genre>>()
            {
                @Override
                public void onSuccess(Method method, List<Genre> response)
                {
                    consumer.accept(response);
                    view.genreDisplay().setAcceptableValues(response);
                }
            });
    }

    private void loadData(String id, Consumer<Book> consumer)
    {
        BookService.getInstance().findOne(id, new AbstractMethodCallback<Book>()
        {
            @Override
            public void onSuccess(Method method, Book response)
            {
                consumer.accept(response);
            }

            @Override
            public void onFailure(Method method, Throwable exception)
            {
                super.onFailure(method, exception);
                consumer.accept(null);
            }
        });
    }

    private void saveData(Book genre, Consumer<Void> consumer)
    {
        BookService service = BookService.getInstance();

        if (genre.getId() != null)
        {
            service.update(id.toString(), genre,
                new AbstractMethodCallback<Void>()
                {
                    @Override
                    public void onSuccess(Method method, Void response)
                    {
                        consumer.accept(null);
                    }
                });
        }
        else
        {
            service.create(genre, new AbstractMethodCallback<Integer>()
            {
                @Override
                public void onSuccess(Method method, Integer response)
                {
                    consumer.accept(null);
                }
            });
        }
    }
}