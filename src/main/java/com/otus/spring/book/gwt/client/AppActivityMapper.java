package com.otus.spring.book.gwt.client;


import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;
import com.otus.spring.book.gwt.client.form.author.edit.AuthorEditActivity;
import com.otus.spring.book.gwt.client.form.author.list.AuthorListActivity;
import com.otus.spring.book.gwt.client.form.book.comments.BookCommentsActivity;
import com.otus.spring.book.gwt.client.form.book.edit.BookEditActivity;
import com.otus.spring.book.gwt.client.form.book.list.BookListActivity;
import com.otus.spring.book.gwt.client.form.genre.edit.GenreEditActivity;
import com.otus.spring.book.gwt.client.form.genre.list.GenreListActivity;
import com.otus.spring.book.gwt.client.hello.HelloActivity;
import com.otus.spring.book.gwt.client.place.AuthorEditPlace;
import com.otus.spring.book.gwt.client.place.AuthorListPlace;
import com.otus.spring.book.gwt.client.place.BookCommentsPlace;
import com.otus.spring.book.gwt.client.place.BookEditPlace;
import com.otus.spring.book.gwt.client.place.BookListPlace;
import com.otus.spring.book.gwt.client.place.GenreEditPlace;
import com.otus.spring.book.gwt.client.place.GenreListPlace;
import com.otus.spring.book.gwt.client.place.HelloPlace;


public class AppActivityMapper implements ActivityMapper
{
    private ClientFactory clientFactory;

    public AppActivityMapper(ClientFactory clientFactory)
    {
        super();
        this.clientFactory = clientFactory;
    }

    @Override
    public Activity getActivity(Place place)
    {
        if (place instanceof HelloPlace)
            return new HelloActivity((HelloPlace) place, clientFactory);
        else if (place instanceof GenreListPlace)
            return new GenreListActivity((GenreListPlace) place, clientFactory);
        else if (place instanceof AuthorListPlace)
            return new AuthorListActivity((AuthorListPlace) place, clientFactory);
        else if (place instanceof BookListPlace)
            return new BookListActivity((BookListPlace) place, clientFactory);
        else if (place instanceof GenreEditPlace)
            return new GenreEditActivity((GenreEditPlace) place, clientFactory);
        else if (place instanceof AuthorEditPlace)
            return new AuthorEditActivity((AuthorEditPlace) place, clientFactory);
        else if (place instanceof BookEditPlace)
            return new BookEditActivity((BookEditPlace) place, clientFactory);
        else if (place instanceof BookCommentsPlace)
            return new BookCommentsActivity((BookCommentsPlace) place, clientFactory);
        return null;
    }
}