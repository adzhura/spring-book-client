package com.otus.spring.book.gwt.client.bean;


import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"id", "name"})
@ToString(of = {"id", "name", "genre", "author"})
public class Book implements NamedObject<String>
{
    private String id;

    private String name;

    private Author author;

    private Genre genre;

    private List<Comment> comments;
}
