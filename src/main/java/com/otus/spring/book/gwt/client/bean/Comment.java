package com.otus.spring.book.gwt.client.bean;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id", "userName", "text"})
@EqualsAndHashCode(of = {"id", "userName", "text"})
public class Comment implements HasId<Integer>
{
    private Integer id;

    private String userName;

    private String text;
}
