package com.otus.spring.book.gwt.client;


import com.google.gwt.place.shared.PlaceController;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.otus.spring.book.gwt.client.form.author.edit.AuthorEditView;
import com.otus.spring.book.gwt.client.form.author.edit.AuthorEditViewImpl;
import com.otus.spring.book.gwt.client.form.author.list.AuthorListView;
import com.otus.spring.book.gwt.client.form.author.list.AuthorListViewImpl;
import com.otus.spring.book.gwt.client.form.book.comments.BookCommentsView;
import com.otus.spring.book.gwt.client.form.book.comments.BookCommentsViewImpl;
import com.otus.spring.book.gwt.client.form.book.edit.BookEditView;
import com.otus.spring.book.gwt.client.form.book.edit.BookEditViewImpl;
import com.otus.spring.book.gwt.client.form.book.list.BookListView;
import com.otus.spring.book.gwt.client.form.book.list.BookListViewImpl;
import com.otus.spring.book.gwt.client.form.genre.edit.GenreEditView;
import com.otus.spring.book.gwt.client.form.genre.edit.GenreEditViewImpl;
import com.otus.spring.book.gwt.client.form.genre.list.GenreListView;
import com.otus.spring.book.gwt.client.form.genre.list.GenreListViewImpl;
import com.otus.spring.book.gwt.client.hello.HelloView;
import com.otus.spring.book.gwt.client.hello.HelloViewImpl;


public class ClientFactory
{
    private final EventBus eventBus = new SimpleEventBus();
    private final PlaceController placeController = new PlaceController(
        eventBus);
    private final HelloView helloView = new HelloViewImpl();
    // private final GenreListView genreListView = new GenreListViewImpl();
    // private final AuthorListView authorListView = new AuthorListViewImpl();
    // private final BookListView bookListView = new BookListViewImpl();
    // private final GenreEditView genreEditView = new GenreEditViewImpl();
    // private final AuthorEditView authorEditView = new AuthorEditViewImpl();
    // private final BookEditView bookEditView = new BookEditViewImpl();
    // private final BookCommentsView bookCommentsView = new
    // BookCommentsViewImpl();

    public EventBus getEventBus()
    {
        return eventBus;
    }

    public PlaceController getPlaceController()
    {
        return placeController;
    }

    public HelloView getHelloView()
    {
        return helloView;
    }

    public GenreListView getGenreListView()
    {
        return new GenreListViewImpl();
    }

    public AuthorListView getAuthorListView()
    {
        return new AuthorListViewImpl();
    }

    public BookListView getBookListView()
    {
        return new BookListViewImpl();
    }

    public GenreEditView getGenreEditView()
    {
        return new GenreEditViewImpl();
    }

    public AuthorEditView getAuthorEditView()
    {
        return new AuthorEditViewImpl();
    }

    public BookEditView getBookEditView()
    {
        return new BookEditViewImpl();
    }

    public BookCommentsView getBookCommentsView()
    {
        return new BookCommentsViewImpl();
    }
}
