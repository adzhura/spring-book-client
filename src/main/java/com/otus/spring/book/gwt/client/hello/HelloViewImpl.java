package com.otus.spring.book.gwt.client.hello;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.otus.spring.book.gwt.client.place.AuthorListPlace;
import com.otus.spring.book.gwt.client.place.BookListPlace;
import com.otus.spring.book.gwt.client.place.GenreListPlace;


public class HelloViewImpl extends Composite implements HelloView
{
    private static HelloViewImplUiBinder uiBinder = GWT
        .create(HelloViewImplUiBinder.class);

    interface HelloViewImplUiBinder extends UiBinder<Widget, HelloViewImpl>
    {
    }

    @UiField
    Anchor genreLink;
    @UiField
    Anchor authorLink;
    @UiField
    Anchor bookLink;
    
    private Presenter presenter;

    public HelloViewImpl()
    {
        initWidget(uiBinder.createAndBindUi(this));
    }

    @UiHandler("genreLink")
    void onClickGenre(ClickEvent e)
    {
        presenter.goTo(new GenreListPlace());
    }
    @UiHandler("authorLink")
    void onClickAuthor(ClickEvent e)
    {
        presenter.goTo(new AuthorListPlace());
    }
    @UiHandler("bookLink")
    void onClickBook(ClickEvent e)
    {
        presenter.goTo(new BookListPlace());
    }

    @Override
    public void setPresenter(Presenter presenter)
    {
        this.presenter = presenter;
    }
}