package com.otus.spring.book.gwt.client.bean;


public interface HasId<T>
{
    T getId();

    void setId(T id);
}
