package com.otus.spring.book.gwt.client.form.author.edit;


import java.util.function.Consumer;

import org.fusesource.restygwt.client.Method;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.otus.spring.book.gwt.client.ClientFactory;
import com.otus.spring.book.gwt.client.bean.Author;
import com.otus.spring.book.gwt.client.place.AuthorEditPlace;
import com.otus.spring.book.gwt.client.place.AuthorListPlace;
import com.otus.spring.book.gwt.client.service.AuthorService;
import com.otus.spring.book.gwt.client.template.service.AbstractMethodCallback;


public class AuthorEditActivity extends AbstractActivity
{
    private ClientFactory clientFactory;
    private String id;

    public AuthorEditActivity(AuthorEditPlace place,
        ClientFactory clientFactory)
    {
        this.clientFactory = clientFactory;
        id = place.getId();
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus)
    {
        AuthorEditView view = clientFactory.getAuthorEditView();
        containerWidget.setWidget(view.asWidget());

        view.idDisplay().setValue(null);
        view.firstNameDisplay().setValue(null);
        view.lastNameDisplay().setValue(null);

        if (id != null && !id.trim().isEmpty())
        {
            loadData(id, author -> {
                view.saveEnabled().setEnabled(author != null);

                if (author != null)
                {
                    view.idDisplay().setValue(author.getId());
                    view.firstNameDisplay().setValue(author.getFirstName());
                    view.lastNameDisplay().setValue(author.getLastName());
                }
            });
        }

        view.saveAction().addClickHandler(new ClickHandler()
        {
            @Override
            public void onClick(ClickEvent event)
            {
                String id = view.idDisplay().getValue();
                id = "".equals(id) ? null : id;

                Author author = new Author(id,
                    view.firstNameDisplay().getValue(),
                    view.lastNameDisplay().getValue());

                saveData(author, r -> clientFactory.getPlaceController()
                    .goTo(new AuthorListPlace()));
            }
        });
    }

    private void loadData(String id, Consumer<Author> consumer)
    {
        AuthorService.getInstance().findOne(id,
            new AbstractMethodCallback<Author>()
            {
                @Override
                public void onSuccess(Method method, Author response)
                {
                    consumer.accept(response);
                }

                @Override
                public void onFailure(Method method, Throwable exception)
                {
                    super.onFailure(method, exception);
                    consumer.accept(null);
                }
            });
    }

    private void saveData(Author author, Consumer<Void> consumer)
    {
        AuthorService service = AuthorService.getInstance();

        if (author.getId() != null)
        {
            service.update(id.toString(), author,
                new AbstractMethodCallback<Void>()
                {
                    @Override
                    public void onSuccess(Method method, Void response)
                    {
                        consumer.accept(null);
                    }
                });
        }
        else
        {
            service.create(author, new AbstractMethodCallback<Integer>()
            {
                @Override
                public void onSuccess(Method method, Integer response)
                {
                    consumer.accept(null);
                }
            });
        }
    }
}