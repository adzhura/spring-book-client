package com.otus.spring.book.gwt.client.form.book.list;


import com.otus.spring.book.gwt.client.bean.Book;
import com.otus.spring.book.gwt.client.template.list.ListView;


public interface BookListView extends ListView<Book>
{
}