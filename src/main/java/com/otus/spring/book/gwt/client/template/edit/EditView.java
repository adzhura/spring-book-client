package com.otus.spring.book.gwt.client.template.edit;


import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.IsWidget;


public interface EditView extends IsWidget
{
    HasClickHandlers saveAction();
    
    HasEnabled saveEnabled();
}