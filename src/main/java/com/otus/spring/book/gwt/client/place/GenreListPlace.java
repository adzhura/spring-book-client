package com.otus.spring.book.gwt.client.place;


import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.shared.Prefix;


public class GenreListPlace extends Place
{
    public GenreListPlace()
    {
    }

    @Prefix(value = "genres")
    public static class Tokenizer implements PlaceTokenizer<GenreListPlace>
    {
        @Override
        public String getToken(GenreListPlace place)
        {
            return "";
        }

        @Override
        public GenreListPlace getPlace(String token)
        {
            return new GenreListPlace();
        }
    }
}