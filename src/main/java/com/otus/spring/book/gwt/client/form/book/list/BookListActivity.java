package com.otus.spring.book.gwt.client.form.book.list;


import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.fusesource.restygwt.client.Method;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.otus.spring.book.gwt.client.ClientFactory;
import com.otus.spring.book.gwt.client.bean.Book;
import com.otus.spring.book.gwt.client.place.BookEditPlace;
import com.otus.spring.book.gwt.client.place.BookListPlace;
import com.otus.spring.book.gwt.client.service.BookService;
import com.otus.spring.book.gwt.client.template.list.AbstractListActivity;
import com.otus.spring.book.gwt.client.template.service.AbstractMethodCallback;


public class BookListActivity extends AbstractListActivity<Book>
{
    private ClientFactory clientFactory;

    public BookListActivity(BookListPlace place, ClientFactory clientFactory)
    {
        this.clientFactory = clientFactory;
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus)
    {
        BookListView view = clientFactory.getBookListView();
        containerWidget.setWidget(view.asWidget());
        initView(view);
        view.setDeleteHandler(t -> {
            doDelete(t);
        });
        view.setEditHandler(t -> {
            clientFactory.getPlaceController()
                .goTo(new BookEditPlace(t.getId().toString()));
        });
        view.createAction().addClickHandler(new ClickHandler()
        {
            @Override
            public void onClick(ClickEvent event)
            {
                clientFactory.getPlaceController().goTo(new BookEditPlace());
            }
        });
    }

    @Override
    protected void doDelete(Book book)
    {
        BookService.getInstance().delete(book.getId().toString(),
            new AbstractMethodCallback<Void>()
            {
                @Override
                public void onSuccess(Method method, Void response)
                {
                    BookListActivity.super.doDelete(book);
                }
            });
    }

    @Override
    protected void loadData(Consumer<List<Book>> consumer)
    {
        BookService.getInstance().findAll(new AbstractMethodCallback<List<Book>>()
        {
            @Override
            public void onSuccess(Method method, List<Book> response)
            {
                consumer.accept(response);
            }
            
            @Override
            public void onFailure(Method method, Throwable exception)
            {
                super.onFailure(method, exception);
                consumer.accept(new ArrayList<>());
            }
        });
    }

    @Override
    protected boolean testBean(String text, Book bean)
    {
        return bean.getId().toString().contains(text)
            || bean.getName().toString().contains(text)
            || bean.getAuthor().getFirstName().toString().contains(text)
            || bean.getAuthor().getLastName().toString().contains(text)
            || bean.getGenre().getName().toString().contains(text);
    }

    public void goTo(Place place)
    {
        clientFactory.getPlaceController().goTo(place);
    }
}