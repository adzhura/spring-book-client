package com.otus.spring.book.gwt.client.form.author.list;


import com.otus.spring.book.gwt.client.bean.Author;
import com.otus.spring.book.gwt.client.template.list.ListView;


public interface AuthorListView extends ListView<Author>
{
}