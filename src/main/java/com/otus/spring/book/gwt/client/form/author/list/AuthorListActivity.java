package com.otus.spring.book.gwt.client.form.author.list;


import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.fusesource.restygwt.client.Method;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.otus.spring.book.gwt.client.ClientFactory;
import com.otus.spring.book.gwt.client.bean.Author;
import com.otus.spring.book.gwt.client.place.AuthorEditPlace;
import com.otus.spring.book.gwt.client.place.AuthorListPlace;
import com.otus.spring.book.gwt.client.service.AuthorService;
import com.otus.spring.book.gwt.client.template.list.AbstractListActivity;
import com.otus.spring.book.gwt.client.template.service.AbstractMethodCallback;


public class AuthorListActivity extends AbstractListActivity<Author>
{
    private ClientFactory clientFactory;

    public AuthorListActivity(AuthorListPlace place,
        ClientFactory clientFactory)
    {
        this.clientFactory = clientFactory;
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus)
    {
        AuthorListView view = clientFactory.getAuthorListView();
        containerWidget.setWidget(view.asWidget());
        initView(view);
        view.setDeleteHandler(t -> {
            doDelete(t);
        });
        view.setEditHandler(t -> {
            clientFactory.getPlaceController()
                .goTo(new AuthorEditPlace(t.getId().toString()));
        });
        view.createAction().addClickHandler(new ClickHandler()
        {
            @Override
            public void onClick(ClickEvent event)
            {
                clientFactory.getPlaceController().goTo(new AuthorEditPlace());
            }
        });
    }

    @Override
    protected void doDelete(Author author)
    {
        AuthorService.getInstance().delete(author.getId().toString(),
            new AbstractMethodCallback<Void>()
            {
                @Override
                public void onSuccess(Method method, Void response)
                {
                    AuthorListActivity.super.doDelete(author);
                }
            });
    }

    @Override
    protected void loadData(Consumer<List<Author>> consumer)
    {
        AuthorService.getInstance().findAll(new AbstractMethodCallback<List<Author>>()
        {
            @Override
            public void onSuccess(Method method, List<Author> response)
            {
                consumer.accept(response);
            }
            
            @Override
            public void onFailure(Method method, Throwable exception)
            {
                super.onFailure(method, exception);
                consumer.accept(new ArrayList<>());
            }
        });
    }

    @Override
    protected boolean testBean(String text, Author bean)
    {
        return bean.getId().toString().contains(text)
            || bean.getFirstName().toString().contains(text)
            || bean.getLastName().toString().contains(text);
    }

    public void goTo(Place place)
    {
        clientFactory.getPlaceController().goTo(place);
    }
}