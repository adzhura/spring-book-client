package com.otus.spring.book.gwt.client.form.genre.edit;


import java.util.function.Consumer;

import org.fusesource.restygwt.client.Method;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.otus.spring.book.gwt.client.ClientFactory;
import com.otus.spring.book.gwt.client.bean.Genre;
import com.otus.spring.book.gwt.client.place.GenreEditPlace;
import com.otus.spring.book.gwt.client.place.GenreListPlace;
import com.otus.spring.book.gwt.client.service.GenreService;
import com.otus.spring.book.gwt.client.template.service.AbstractMethodCallback;


public class GenreEditActivity extends AbstractActivity
{
    private ClientFactory clientFactory;
    private String id;

    public GenreEditActivity(GenreEditPlace place, ClientFactory clientFactory)
    {
        this.clientFactory = clientFactory;
        id = place.getId();
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus)
    {
        GenreEditView view = clientFactory.getGenreEditView();
        containerWidget.setWidget(view.asWidget());

        view.idDisplay().setValue(null);
        view.nameDisplay().setValue(null);

        if (id != null && !id.trim().isEmpty())
        {
            loadData(id, genre -> {
                view.saveEnabled().setEnabled(genre != null);

                if (genre != null)
                {
                    view.idDisplay().setValue(genre.getId());
                    view.nameDisplay().setValue(genre.getName());
                }
            });
        }

        view.saveAction().addClickHandler(new ClickHandler()
        {
            @Override
            public void onClick(ClickEvent event)
            {
                String id = view.idDisplay().getValue();
                id = "".equals(id) ? null : id;

                Genre genre = new Genre(id, view.nameDisplay().getValue());

                saveData(genre, r -> clientFactory.getPlaceController()
                    .goTo(new GenreListPlace()));
            }
        });
    }

    private void loadData(String id, Consumer<Genre> consumer)
    {
        GenreService.getInstance().findOne(id,
            new AbstractMethodCallback<Genre>()
            {
                @Override
                public void onSuccess(Method method, Genre response)
                {
                    consumer.accept(response);
                }

                @Override
                public void onFailure(Method method, Throwable exception)
                {
                    super.onFailure(method, exception);
                    consumer.accept(null);
                }
            });
    }

    private void saveData(Genre genre, Consumer<Void> consumer)
    {
        GenreService service = GenreService.getInstance();

        if (genre.getId() != null)
        {
            service.update(id.toString(), genre,
                new AbstractMethodCallback<Void>()
                {
                    @Override
                    public void onSuccess(Method method, Void response)
                    {
                        consumer.accept(null);
                    }
                });
        }
        else
        {
            service.create(genre, new AbstractMethodCallback<Integer>()
            {
                @Override
                public void onSuccess(Method method, Integer response)
                {
                    consumer.accept(null);
                }
            });
        }
    }
}