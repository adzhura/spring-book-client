package com.otus.spring.book.gwt.client.form.genre.list;


import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.fusesource.restygwt.client.Method;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.otus.spring.book.gwt.client.ClientFactory;
import com.otus.spring.book.gwt.client.bean.Genre;
import com.otus.spring.book.gwt.client.place.GenreEditPlace;
import com.otus.spring.book.gwt.client.place.GenreListPlace;
import com.otus.spring.book.gwt.client.service.GenreService;
import com.otus.spring.book.gwt.client.template.list.AbstractListActivity;
import com.otus.spring.book.gwt.client.template.service.AbstractMethodCallback;


public class GenreListActivity extends AbstractListActivity<Genre>
{
    private ClientFactory clientFactory;

    public GenreListActivity(GenreListPlace place, ClientFactory clientFactory)
    {
        this.clientFactory = clientFactory;
    }

    @Override
    public void start(AcceptsOneWidget containerWidget, EventBus eventBus)
    {
        GenreListView view = clientFactory.getGenreListView();
        containerWidget.setWidget(view.asWidget());
        initView(view);
        view.setDeleteHandler(t -> {
            doDelete(t);
        });
        view.setEditHandler(t -> {
            clientFactory.getPlaceController()
                .goTo(new GenreEditPlace(t.getId().toString()));
        });
        view.createAction().addClickHandler(new ClickHandler()
        {
            @Override
            public void onClick(ClickEvent event)
            {
                clientFactory.getPlaceController().goTo(new GenreEditPlace());
            }
        });
    }

    @Override
    protected void doDelete(Genre genre)
    {
        GenreService.getInstance().delete(genre.getId().toString(),
            new AbstractMethodCallback<Void>()
            {
                @Override
                public void onSuccess(Method method, Void response)
                {
                    GenreListActivity.super.doDelete(genre);
                }
            });
    }

    @Override
    protected void loadData(Consumer<List<Genre>> consumer)
    {
        GenreService.getInstance()
            .findAll(new AbstractMethodCallback<List<Genre>>()
            {
                @Override
                public void onSuccess(Method method, List<Genre> response)
                {
                    consumer.accept(response);
                }

                @Override
                public void onFailure(Method method, Throwable exception)
                {
                    super.onFailure(method, exception);
                    consumer.accept(new ArrayList<>());
                }
            });
    }

    @Override
    protected boolean testBean(String text, Genre bean)
    {
        return bean.getId().toString().contains(text)
            || bean.getName().toString().contains(text);
    }
}