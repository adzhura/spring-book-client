package com.otus.spring.book.gwt.client.service;


import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestService;
import org.fusesource.restygwt.client.RestServiceProxy;

import com.google.gwt.core.client.GWT;
import com.otus.spring.book.gwt.client.bean.Comment;


@Path("/genres")
public interface CommentService extends RestService
{
    public static CommentService getInstance()
    {
        Resource resource = new Resource("http://localhost:8181/books");

        CommentService service = GWT.create(CommentService.class);
        ((RestServiceProxy) service).setResource(resource);

        return service;
    }

    @POST
    @Path("/{id}/comments")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addComment(@PathParam("id") String id, Comment comment,
        MethodCallback<Void> callback);
}
