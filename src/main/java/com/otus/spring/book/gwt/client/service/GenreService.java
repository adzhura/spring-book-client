package com.otus.spring.book.gwt.client.service;


import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestService;
import org.fusesource.restygwt.client.RestServiceProxy;

import com.google.gwt.core.client.GWT;
import com.otus.spring.book.gwt.client.bean.Genre;


@Path("/genres")
public interface GenreService extends RestService
{
    public static GenreService getInstance()
    {
        Resource resource = new Resource("http://localhost:8181/genres");

        GenreService service = GWT.create(GenreService.class);
        ((RestServiceProxy) service).setResource(resource);

        return service;
    }

    @GET
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public void findAll(MethodCallback<List<Genre>> callback);

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void findOne(@PathParam("id") String id,
        MethodCallback<Genre> callback);

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Genre genre, MethodCallback<Integer> callback);

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void update(@PathParam("id") String id, Genre genre,
        MethodCallback<Void> callback);

    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") String id,
        MethodCallback<Void> callback);
}
