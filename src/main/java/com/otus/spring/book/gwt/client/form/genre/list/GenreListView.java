package com.otus.spring.book.gwt.client.form.genre.list;


import com.otus.spring.book.gwt.client.bean.Genre;
import com.otus.spring.book.gwt.client.template.list.ListView;


public interface GenreListView extends ListView<Genre>
{
}